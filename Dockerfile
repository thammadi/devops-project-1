FROM openjdk:17
RUN groupadd --gid 1000 java \
&& useradd --uid 1000 --gid java --create-home java
Run curl --version
USER java
EXPOSE 8080
VOLUME /tmp
WORKDIR /app
COPY ./target/spring-petclinic-3.0.0-SNAPSHOT.jar /app/spring-petclinic-3.0.0-SNAPSHOT.jar

CMD ["java" ,"-jar", "/app/spring-petclinic-3.0.0-SNAPSHOT.jar"]
